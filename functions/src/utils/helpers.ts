//for checking empty string or not
import * as admin from "firebase-admin";
import * as functions from "firebase-functions";


const db = admin.firestore();


const isEmpty = (string: String) => {
    if (string.trim() === '') {
        return true;
    } else {
        return false;
    }
}
//for checking valid email address with regualr expression
const isEmail = (email: String) => {
    const regx = /^(([^<>()\[\]\\.,;:\s@”]+(\.[^<>()\[\]\\.,;:\s@”]+)*)|(“.+”))@((\[[0–9]{1,3}\.[0–9]{1,3}\.[0–9]{1,3}\.[0–9]{1,3}\])|(([a-zA-Z\-0–9]+\.)+[a-zA-Z]{2,}))$/;
    if (email.match(regx)) {
        return true
    } else {
        return false
    }
}
//for validating signup data
export function validateSignUPData(data: any) {


    let errors: any = {};
    if (isEmpty(data.email)) {
        errors.email = "Email Filed Is Required!";
    } else if (!isEmail(data.email)) {
        errors.email = "Must be valid email address";
    }
    if (data.password !== data.confirmPassword) {
        errors.confirmPassword = "Password Did not match";
    }
    return {
        errors,
        valid: Object.keys(errors).length === 0 ? true : false
    };
}
//for validating signin data
export function validateLoginData(data: any) {
    let errors: any = {};
    if (isEmpty(data.email)) {
        errors.email = "Email filed is required!";
    } else if (isEmpty(data.password)) {
        errors.password = "Password filed is required!";
    } else if (!isEmail(data.email)) {
        errors.email = "Must be valid email address";
    }
    return {
        errors,
        valid: Object.keys(errors).length === 0 ? true : false
    };
}

export function checkAuthorization(req: any){
    var authorization = req.headers.authorization ?? "";

    if (authorization == "") {
        throw Error("no authorization found");
    }

    return admin
        .auth()
        .verifyIdToken(authorization)
        .then(async (decodedToken) => {



            var query = db.collection('users').doc(decodedToken.uid);

            var user = await query.get();

            functions.logger.log(`user recovered: ${user.data()?.userId}`);


            return user.data();
        }).catch(err=>{throw Error("user not found")});
}





