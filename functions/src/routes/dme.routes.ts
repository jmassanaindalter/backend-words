import { Router } from "express";
import * as admin from "firebase-admin";
import { DmePlayer } from "../models/dme/player";
import { DmeTeam } from "../models/dme/team";
//import * as functions from "firebase-functions";
import { checkAuthorization } from "../utils/helpers";

const router = Router();
const db = admin.firestore();


router.post("/api/dme/team", async (req, res) => {
    checkAuthorization(req).then(async (user: any) => {

        try {


            var docRef = await db.collection('teams').doc()

            req.body.owner = user.userId;

            var team = new DmeTeam(req.body);


            for(var i = 0; i < team.players.length;i++){

                var playerId = team.players[i];
                var doc = await db.collection("players").doc(playerId);
                var data = await doc.get();
                if(!data.exists){
                    return res.status(500).json({ "error": "Trying to add not existing player" });

            }



            }

            var saveObject = JSON.parse(JSON.stringify(team));
            
            await docRef.create(saveObject);


            return res.status(200).json({ "message": "Team created successfully" });
        } catch (error) {


            return res.status(500).json({ "error": JSON.stringify(error) });

        }
    }, error => {

        return res.status(500).json({ "error": error });

    })
});



router.get('/api/dme/createPlayers',async (req,res)=>{
    checkAuthorization(req).then(async (user:any)=>{
        try{

            for(var i = 0; i < 100;i++){
                const docRef  = db.collection('players').doc();


                var player = new DmePlayer({
                    playerId:"",
                    name:DmePlayer.generateName(),
                    nickname:DmePlayer.generateName(),
                    age:DmePlayer.getRandomInt(18,29) ,
                    kda:`${DmePlayer.getRandomInt(1,8)}/${DmePlayer.getRandomInt(1,4)}/${DmePlayer.getRandomInt(3,15)}`,
                    team:DmePlayer.getRandomTeam(), //ID DEL EQUIPO
                    role:DmePlayer.getRandomRole(),
                    category:DmePlayer.getRandomCategory(),
                }); 

                var saveObject = JSON.parse(JSON.stringify(player));


                await docRef.create(saveObject);
                
                await docRef.update({
                    playerId: docRef.id
                });


            }
    
           
    
            return res.status(200).send();
        }catch(error){
            return res.status(500).send(error);
        }

    }).catch((error)=>{
            res.status(500).json({"error":"User not found"});
    });

    
});

router.get('/api/dme/players',async (req,res)=>{
    checkAuthorization(req).then(async (user:any)=>{
        try{
            const query  = db.collection('players');
    
            const querySnapshot = await query.get()
    
            const docs = querySnapshot.docs;
    
            const response = docs.map(doc=>(doc.data()));
    
            return res.status(200).json(response);
        }catch(error){
            return res.status(500).send(error);
        }

    }).catch((error)=>{
            res.status(500).json({"error":"User not found"});
    });

    
});


router.get('/api/dme/team',async (req,res)=>{
    checkAuthorization(req).then(async (user:any)=>{
        try{
            const query  = db.collection('teams');
    
            const querySnapshot = await query.get()
    
            const docs = querySnapshot.docs;
    
            const response = docs.map(doc=>(doc.data()));

            var populatedResponse = [];
            for(var i = 0; i < response.length;i++){

                var team = response[i];

                for(var j = 0; j < team.players.length; j++){
                    var playerRef =  db.collection('players').doc(team.players[j]);
                    var playerGet = await playerRef.get();
                    var playerData = await playerGet.data();
                    team.playersPopulated.push(playerData);

                }

                populatedResponse.push(team);

            }
    
            return res.status(200).json(populatedResponse);
        }catch(error){
            return res.status(500).send(error);
        }

    }).catch((error)=>{
            res.status(500).json({"error":"User not found"});
    });

    
});

router.get('/api/dme/players/:category',async (req,res)=>{
    checkAuthorization(req).then(async (user:any)=>{
        try{
            const query  = db.collection('players').where("category","==",req.params.category);
    
            const querySnapshot = await query.get()
    
            const docs = querySnapshot.docs;
    
            const response = docs.map(doc=>(doc.data()));
    
            return res.status(200).json(response);
        }catch(error){
            return res.status(500).send(error);
        }

    }).catch((error)=>{
            res.status(500).json({"error":"User not found"});
    });

    
});






// BOOKS SAMPLES








module.exports = router;