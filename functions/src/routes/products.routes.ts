import {Router} from "express";
import * as admin from "firebase-admin";
import * as functions from "firebase-functions";
import { checkAuthorization } from "../utils/helpers";

const router = Router();

const db = admin.firestore();


router.post('/api/products',async (req,res)=>{ 

    

    try{
        var docRef = await db.collection('products').doc()
        await docRef.create({name:req.body.name}); 

        var response = await docRef.get();

        return res.json(204).json(response.id);    

    }catch(error){
        return res.status(500).send(error);
    }
    

});


router.get("/api/products/:product_id",async (req,res)=>{
    try{

        console.log("req.params.product_id:",req.params.product_id);

        const doc = await db.collection('products').doc(req.params.product_id)
        const item = await doc.get();
        const response = item.data();

        return res.status(200).json(response);
    }catch(error){
        return res.status(500).send(error);

    }
})


router.get("/api/products/",async (req,res)=>{

    checkAuthorization(req).then(async (user:any)=>{

        functions.logger.log("FOUND USER:",user);

            try{
                const query  = db.collection('products');
        
                const querySnapshot = await query.get()
        
                const docs = querySnapshot.docs;
        
                const response = docs.map(doc=>({
                    id:doc.id,
                    name:doc.data().name
                }));
        
                return res.status(200).json(response);
            }catch(error){
                return res.status(500).send(error);
            }
            // ...
        })  
        .catch((error:Error) => {
            functions.logger.log("ERROR IS:",error);
            return res.status(500).json({"error":error.message});
            // Handle error
        });


    
})


router.delete("/api/products/:product_id",async (req,res)=>{
    try{


        const doc = await db.collection('products').doc(req.params.product_id)
        await doc.delete();

        return res.status(200).json();
    }catch(error){
        return res.status(500).send(error);

    }
})


router.put("/api/products/:product_id",async (req,res)=>{
    try{


        const doc = await db.collection('products').doc(req.params.product_id)
        await doc.update({
            name: req.body.name
        });

        return res.status(200).json();
    }catch(error){
        return res.status(500).send(error);

    }
})


module.exports = router;