import { Router } from "express";
import * as admin from "firebase-admin";
import * as functions from "firebase-functions";
import { checkAuthorization } from "../utils/helpers";

const router = Router();
const db = admin.firestore();




router.get('/api/book/users/:code',async (req,res)=>{

    checkAuthorization(req).then(async (user:any)=>{

        functions.logger.log("FOUND USER:",user);

            try{
                const query  = db.collection('users');
        
                const querySnapshot = await query.get()
        
                const docs = querySnapshot.docs;
        
                const response = docs.map(doc=>(doc.data()));
                var filteredResponse:any = [];
                response.forEach((el)=>{
                    if(el.books != null){
                        functions.logger.log("EL IS: ",el);

                        var bookFound = el.books.find((book:any)=>book.code == req.params.code)
                        if(bookFound != null){
                            filteredResponse.push(el);
                        }
                    }
                });

                
        
                return res.status(200).json(filteredResponse);
            }catch(error){
                return res.status(500).send(error);
            }
            // ...
        })  
        .catch((error:Error) => {
            functions.logger.log("ERROR IS:",error);
            return res.status(500).json({"error":error.message});
            // Handle error
        });

});


router.get('/api/book/users/',async (req,res)=>{

    checkAuthorization(req).then(async (user:any)=>{

        functions.logger.log("FOUND USER:",user);

            try{
                const query  = db.collection('users').orderBy('books');
        
                const querySnapshot = await query.get()
        
                const docs = querySnapshot.docs;
        
                const response = docs.map(doc=>(doc.data()));
                
                return res.status(200).json(response);
            }catch(error){
                return res.status(500).send(error);
            }
            // ...
        })  
        .catch((error:Error) => {
            functions.logger.log("ERROR IS:",error);
            return res.status(500).json({"error":error.message});
            // Handle error
        });

});



router.post("/api/book/update/",async (req,res)=>{
    checkAuthorization(req).then(async (user:any)=>{
        
        try{
            functions.logger.log(`ERROR: ${user.userId}`);


            const doc = await db.collection('users').doc(user.userId);

            if(!req.body.code || !req.body.owner){
                return res.status(500).json({"error":"No book code passed or the book has no owner"});
            }
            
            var userObject = await doc.get();

            var userData = await userObject.data();

            var books = userData?.books;

            if(books == null){
                books = [];
            }

            var indexBook = books.findIndex((book:any)=>book.code == req.body.code );
            
            if(indexBook >= 0){
                books[indexBook] = req.body;
            }else{
                return res.status(500).json({"error":"Trying to update not owned book"});

            }


            await doc.update({
                books: books
            });
    
            return res.status(200).json({"message":"Book successfully updated"});
        }catch(error){
            
            return res.status(500).json({"error":"Error updating book"});
    
        }
    },error=>{
        return res.status(500).json({"error":"User not found"});

    })
});


router.post("/api/book/share/:code/:receiver",async (req,res)=>{
    checkAuthorization(req).then(async (user:any)=>{
        
        try{
            functions.logger.log(`ERROR: ${user.userId}`);

            var bookToShare = user.books.find((book:any)=>book.code == req.params.code);

            if(!bookToShare){
                return res.status(500).json({"error":"You don't own this book"});
            }

            const doc = await db.collection('users').doc(req.params.receiver);
            
            var userObject = await doc.get();

            var userData = await userObject.data();

            var books = userData?.sharedBooks;

            if(books == null){
                books = [];
            }

            if(books.findIndex((book:any)=>book.code == req.params.code && book.owner == bookToShare.owner) >= 0){
                return res.status(500).json({"error":"Book already shared to receiver"});

            }else{
                books.push(bookToShare);
            }


            await doc.update({
                sharedBooks: books
            });
    
            return res.status(200).json({"message":"Book successfully added"});
        }catch(error){
            
            return res.status(500).json({"error":"Error adding book"});
    
        }
    },error=>{
        return res.status(500).json({"error":"User not found"});

    })
});


router.post("/api/book/add",async (req,res)=>{
    checkAuthorization(req).then(async (user:any)=>{
        
        try{
            functions.logger.log(`ERROR: ${user.userId}`);

            req.body.owner = user.userId;

            const doc = await db.collection('users').doc(user.userId);

            if(!req.body.code){
                return res.status(500).json({"error":"No book code passed"});
            }
            
            var userObject = await doc.get();

            var userData = await userObject.data();

            var books = userData?.books;

            if(books == null){
                books = [];
            }

            if(books.findIndex((book:any)=>book.code == req.body.code) >= 0){
                return res.status(500).json({"error":"Book already added"});

            }else{
                books.push(req.body);
            }


            await doc.update({
                books: books
            });
    
            return res.status(200).json({"message":"Book successfully added"});
        }catch(error){
            
            return res.status(500).json({"error":"Error adding book"});
    
        }
    },error=>{
        return res.status(500).json({"error":"User not found"});

    })
});


router.post("/api/book/remove/:code",async (req,res)=>{
    checkAuthorization(req).then(async (user:any)=>{
        
        try{
            functions.logger.log(`ERROR: ${user.userId}`);

            req.body.owner = user.userId;

            const doc = await db.collection('users').doc(user.userId);

            
            var userObject = await doc.get();

            var userData = await userObject.data();

            var books:any[] = userData?.books;

            if(books == null){
                return res.status(500).json({"error":"Your book collection is empty"});
            }

            var bookIndex = books.findIndex((book:any)=>book.code == req.params.code);
            if(bookIndex < 0){
                return res.status(500).json({"error":"You don't have this book in your collection"});

            }else{
                books.splice(bookIndex,1);
            }


            await doc.update({
                books: books
            });
    
            return res.status(200).json({"message":"Book successfully remove"});
        }catch(error){
            
            return res.status(500).json({"error":"Error removing book"});
    
        }
    },error=>{
        return res.status(500).json({"error":"User not found"});

    })
});







module.exports = router;