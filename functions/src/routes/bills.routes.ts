import { Router } from "express";
import * as admin from "firebase-admin";
import { Bill } from "../models/bills/bill";
//import * as functions from "firebase-functions";
import { checkAuthorization } from "../utils/helpers";

const router = Router();
const db = admin.firestore();



router.get("/api/bills/", async (req, res) => {
    checkAuthorization(req).then(async (user: any) => {

        try {


            var docRef =  db.collection('bills').where("owner","==",user.userId);
            
            req.body.owner = user.userId;

            var docsList = await docRef.get();

            var bills = docsList.docs.map((el)=>el.data());

            return res.status(200).json(bills);
        } catch (error) {


            return res.status(500).json({ "error": JSON.stringify(error) });

        }
    }, error => {

        return res.status(500).json({ "error": "Auth token invalid" });

    })

});


router.get("/api/bills/search/:reference", async (req, res) => {
    checkAuthorization(req).then(async (user: any) => {

        try {


            var docRef =  db.collection('bills').where("reference","==",req.params.reference);
            
            req.body.owner = user.userId;

            var docsList = await docRef.get();

            var bills = docsList.docs.map((el)=>el.data());

            return res.status(200).json(bills);
        } catch (error) {


            return res.status(500).json({ "error": JSON.stringify(error) });

        }
    }, error => {

        return res.status(500).json({ "error": "Auth token invalid" });

    })

})

router.post("/api/bills/add", async (req, res) => {
    checkAuthorization(req).then(async (user: any) => {

        try {


            var docRef =  db.collection('bills').doc()

            req.body.owner = user.userId;

            var bill = new Bill(req.body);
            var saveObject = JSON.parse(JSON.stringify(bill));
            
            await docRef.create(saveObject);


            return res.status(200).json({ "message": "Bill created successfully" });
        } catch (error) {


            return res.status(500).json({ "error": "unknown error" });

        }
    }, error => {

        return res.status(500).json({ "error": "Auth token invalid" });

    })

});




module.exports = router;