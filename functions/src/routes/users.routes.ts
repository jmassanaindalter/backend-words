import {Router} from "express";
import * as admin from "firebase-admin";
import * as functions from "firebase-functions";
import { checkAuthorization } from "../utils/helpers";

const router = Router();

const db = admin.firestore();


router.post("/api/updatePosition",async (req,res)=>{
    checkAuthorization(req).then(async (user:any)=>{
        
        try{
            functions.logger.log(`ERROR: ${user.userId}`);

            const doc = await db.collection('users').doc(user.userId);

            if(
                !(req.body.lat && req.body.lng) ||
                (isNaN(req.body.lat) || req.body.lat >= 90 || req.body.lat <= -90)
                || (isNaN(req.body.lng) || req.body.lng >= 180 || req.body.lng <= -180)
            ){
                return res.status(500).json({"error":"Lat lng params incorrect"});
            }
            

            await doc.update({
                position: {"lat":0,"lng":0}
            });
    
            return res.status(200).json({"message":"Location updated successfully"});
        }catch(error){
            
            return res.status(500).json({"error":error});
    
        }
    },error=>{
        return res.status(500).json({"error":error});

    })
});




router.put("/api/updatePosition",async (req,res)=>{
    checkAuthorization(req).then(async (user:any)=>{
        
        try{
            functions.logger.log(`ERROR: ${user.userId}`);

            const doc = await db.collection('users').doc(user.userId);

            if(
                !(req.body.lat && req.body.lng) ||
                (isNaN(req.body.lat) || req.body.lat >= 90 || req.body.lat <= -90)
                || (isNaN(req.body.lng) || req.body.lng >= 180 || req.body.lng <= -180)
            ){
                return res.status(500).json({"error":"Lat lng params incorrect"});
            }
            

            await doc.update({
                position: {lat:req.body.lat,lng:req.body.lng}
            });
    
            return res.status(200).json({"message":"Location updated successfully"});
        }catch(error){
            
            return res.status(500).json({"error":error});
    
        }
    },error=>{
        return res.status(500).json({"error":error});

    })
});





router.get("/api/users/me",async (req,res)=>{
    checkAuthorization(req).then(async (user:any)=>{
        res.status(200).json(user);
    })  
    .catch((error:Error) => {
        functions.logger.log("ERROR IS:",error);
        return res.status(500).json({"error":"invalid token auth"});
        // Handle error
    });


});

router.get("/api/users/",async (req,res)=>{

    checkAuthorization(req).then(async (user:any)=>{

        functions.logger.log("FOUND USER:",user);

            try{
                const query  = db.collection('users');
        
                const querySnapshot = await query.get()
        
                const docs = querySnapshot.docs;
        
                const response = docs.map(doc=>(doc.data()));
        
                return res.status(200).json(response);
            }catch(error){
                return res.status(500).send(error);
            }
            // ...
        })  
        .catch((error:Error) => {
            functions.logger.log("ERROR IS:",error);
            return res.status(500).json({"error":error.message});
            // Handle error
        });


    
})









module.exports = router;