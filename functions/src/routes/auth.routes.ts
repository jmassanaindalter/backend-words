import { Router } from "express";
import * as admin from "firebase-admin";
import * as firebase from "firebase/auth";
import * as functions from "firebase-functions";

import { validateLoginData, validateSignUPData } from "../utils/helpers";

const router = Router();
const db = admin.firestore();


router.post('/api/auth/signup', async (req, res) => {

    const newUser = {
        email: req.body.email,
        password: req.body.password,
        confirmPassword: req.body.confirmPassword,
    };

    functions.logger.log("BODY IS:",req.body.email);


    functions.logger.log("DATA IS:",req.body,"USER IS:",newUser);

    const { valid, errors } = validateSignUPData(newUser);
    if (!valid)//checking validation
        return res.status(400).json(errors);
    let token: any, userId: any;

    return db.collection('users').where('email', '==', newUser.email).get()
        .then((result: any) => {
            if (result.docs.length > 0) {
                return res.status(400).json({ handle: 'The user id already taken' });
            } else {
                return firebase
                    .createUserWithEmailAndPassword(
                        firebase.getAuth(), newUser.email, newUser.password)
                    .then((data: any) => {
                        userId = data.user.uid
                        return data.user.getIdToken()
                            .then((idToken: any) => {
                                token = idToken
                                const userCredentials = {
                                    userId,
                                    email: newUser.email,
                                    createdAt: new Date().toISOString(),
                                }
                                db.doc(`/users/${userId}`).set(userCredentials);
                            }).then(() => {
                                return res.status(201).json({ token })
                            })
                    })
            }
        })



})


router.post('/api/auth/signin',  (req, res) => {

    const user = {
        email: req.body.email,
        password: req.body.password
    }
    const { valid, errors } = validateLoginData(user);

    if (!valid) return res.status(400).json(errors);

    try {


        return firebase.signInWithEmailAndPassword(firebase.getAuth(), user.email, user.password)//firebase signin method
            .then(async (data: any) => {
                console.log(JSON.stringify(data))
                return [await data.user.getIdToken(),data.user.uid];
            }).then((params:any[]) => {
                console.log("UID TO RETURN:",params[1]);


                return res.json({ 
                    token: params[0],
                    uid:params[1] 
                });
            }).catch((err: any) => {
                if (err.code == 'auth/wrong-password' || err.code == 'auth/user-not-found') {
                    return res.status(403).json({ message: 'Wrong credentials, Please try again' });
                }
                return res.status(500).json({ error: err.code })
            })

    } catch (error) {
        return res.status(500).json({ error: error })

    }









});





module.exports = router;