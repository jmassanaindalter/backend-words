import { Router } from "express";
import * as admin from "firebase-admin";
import * as functions from "firebase-functions";
import { Match } from "../models/match";
import { checkAuthorization } from "../utils/helpers";

const axios = require('axios');
const router = Router();

const db = admin.firestore();



router.get("/api/game/myMatches", async (req, res) => {
    checkAuthorization(req).then(async (user: any) => {

        functions.logger.log("FOUND USER:", user);
        try {
            const query = db.collection('games').where("owner","==" ,user.userId)
                                                

            const querySnapshot = await query.get()

            const docs = querySnapshot.docs;

            const response = docs.map(doc => {
                 var data = doc.data();
                 data.id = doc.id;
                 data.players = populateUsers(data.players);
                 return data;
            }); 

            return res.status(200).json(response);
        } catch (error) {
            return res.status(500).send(error);
        }
        // ...
    })
    .catch((error: Error) => {
        functions.logger.log("ERROR IS:", error);
        return res.status(500).json({ "error": error.message });
        // Handle error
    });




})


router.get("/api/game/openMatches", async (req, res) => {
    checkAuthorization(req).then(async (user: any) => {

        functions.logger.log("FOUND USER:", user);

        try {
            const query = db.collection('games').where("started_at","==" ,null);

            const querySnapshot = await query.get()

            const docs = querySnapshot.docs;

            const response = docs.map(doc => {
                 var data = doc.data();
                 data.id = doc.id;
                 data.players = populateUsers(data.players);
                 return data;
            });

            return res.status(200).json(response);
        } catch (error) {
            return res.status(500).send(error);
        }
        // ...
    })
    .catch((error: Error) => {
        functions.logger.log("ERROR IS:", error);
        return res.status(500).json({ "error": error.message });
        // Handle error
    });




})


async function populateUsers(players:string[]){

    var usersRef = db.collection('users');

    var mappedUsers = [];
    for (var i = 0; i < players.length; i++) {

        var user = await usersRef.doc(players[i]).get();
        mappedUsers.push(user.data());
    }

    return mappedUsers;
}


function toDateTime(secs:number) {
    var t = new Date(1970, 0, 1); // Epoch
    t.setSeconds(secs);
    return t;
}

router.get("/api/game/:match_id", async (req, res) => {
    checkAuthorization(req).then(async (user: any) => {

        try {

            var doc = await db.collection('games')
                .doc(req.params.match_id);

            var data = await doc.get();

            if (!data.exists) {
                return res.status(404).json({ "error": "Match does not exists" });

            }


            var players = data.data()?.players;

            functions.logger.error(`players: ${players}`);


            var mappedUsers = await populateUsers(players);


            var updatedObject = data.data() ?? {};
            updatedObject.id = data.id;
            
            functions.logger.log("updatedObject.startedAt",updatedObject.started_at);
            if(updatedObject.started_at != null){
                updatedObject.started_at = toDateTime(updatedObject.started_at._seconds);
            }

            if(updatedObject.finished_at != null){
                updatedObject.finished_at = toDateTime(updatedObject.finished_at._seconds);
            }

            updatedObject.players = mappedUsers;
            
            var usersRef = db.collection("users");

            var owner = await usersRef.doc(updatedObject.owner).get();


            updatedObject.owner = owner.data();


            return res.status(200).json(updatedObject);
        } catch (error) {
            functions.logger.error(`ERROR: ${error}`);


            return res.status(500).json({ "error on body": JSON.stringify(error) });

        }

    })
});

router.post("/api/game/validateWord/:match_id/:word", async (req, res) => {
    checkAuthorization(req).then(async (user: any) => {

        try {

            var doc = await db.collection('games')
                .doc(req.params.match_id);

            var data = await doc.get();

            if (!data.exists) {
                return res.status(404).json({ "error": "Match does not exists" });
            }

            if(req.params.match_id == null || req.params.match_id.length < 2){
                return res.status(500).json({ "error": "Invalid query" });
            }

            var words = data.data()!.words;


            

            


            //https://api.dictionaryapi.dev/api/v2/entries/es/hola
            
            return axios.get(`https://api.dictionaryapi.dev/api/v2/entries/es/${req.params.word}`)
            .then((result:any)=>{
                functions.logger.log("RES API: ",result.data);

                //var parseResult = JSON.parse(result.data);
                
                if(result.data?.length > 0){

                    if(words.findIndex((el:any)=>el.word == req.params.word) > -1){
                        return res.status(500).json({ "error": "Word already added" });
                    }

                    words.push({
                        word:req.params.word,
                        points: req.params.word.length
                    });

                    doc.update({
                        words:words
                    });

                    return res.status(200).json({
                        ok:true,
                        points:req.params.word.length
                    });

                }else{
                    return res.status(200).json({
                        ok:false,
                        points:0
                    });
                }
                


            }).catch(()=>{
                return res.status(200).json({
                    ok:false,
                });
            });
            
           
            
            
        } catch (error) {
            functions.logger.error(`ERROR: ${error}`);


            return res.status(500).json({ "error on body": JSON.stringify(error) });

        }


    })
        .catch((error: Error) => {
            functions.logger.log("ERROR IS:", error);
            return res.status(500).json({ "error": error });
            // Handle error
        });
});






router.post("/api/game/validateWord/:match_id", async (req, res) => {
    checkAuthorization(req).then(async (user: any) => {

    })
        .catch((error: Error) => {
            functions.logger.log("ERROR IS:", error);
            return res.status(500).json({ "error": error.message });
            // Handle error
        });
});


router.post("/api/game/checkMatchResult/:match_id", async (req, res) => {
    checkAuthorization(req).then(async (user: any) => {



    })
        .catch((error: Error) => {
            functions.logger.log("ERROR IS:", error);
            return res.status(500).json({ "error": error.message });
            // Handle error
        });
});

router.post("/api/game/join/:match_id", async (req, res) => {
    checkAuthorization(req).then(async (user: any) => {

        try {

            var doc = await db.collection('games')
                .doc(req.params.match_id);

            var data = await doc.get();

            if (!data.exists) {
                return res.status(404).json({ "error": "Match does not exists" });

            }

            var players = data.data()?.players;
            functions.logger.error(`players: ${players}`);

            if (players.indexOf(user.userId) > -1) {
                return res.status(500).json({ "error": "Already joined match" });
            }

            players.push(user.userId);

            doc.update({
                players: players
            })

            return res.status(200).json({ "message": "Match joined successfully" });
        } catch (error) {
            functions.logger.error(`ERROR: ${error}`);


            return res.status(500).json({ "error on body": JSON.stringify(error) });

        }
    }, error => {
        functions.logger.error(`ERROR GETTING USER: ${error}`);

        return res.status(500).json({ "error": error });

    })
});


function getRandomInt(min:number, max:number) {
    min = Math.ceil(min);
    max = Math.floor(max);
    return Math.floor(Math.random() * (max - min )) + min;
}

router.post("/api/game/start/:match_id", async (req, res) => {
    checkAuthorization(req).then(async (user: any) => {

        try {
            functions.logger.log(`ERROR: ${user.userId}`);

            const doc = await db.collection('games').doc(req.params.match_id);

            var docGet = await doc.get();

            if (!docGet.exists) {
                return res.status(404).json({ "error": "Match does not exists" });

            }
            


            var vocs = ["A","E","I","O","U"];
            var cons = ["B", "C", "D", "F", "G", "H", "I", "J", "K", "L", "M", "N", "P", "Q", "R", "S", "T", "V", "W", "X", "Y", "Z"]
            var date = new Date();

            var letters:string [] = [];
            for(var i = 0; i < 6;i++){
                var index = getRandomInt(0,vocs.length);
                functions.logger.log("INDEX",index);
                letters.push(vocs[index]);
            }

            for(var i = 0; i < 10;i++){
                var index = getRandomInt(0,cons.length);
                letters.push(cons[index]);
            }

            functions.logger.log(`lETTERS: ${letters}`);

            await doc.update({
                started: true,
                started_at: date,
                letters:letters
            });



           /* info!.started = true;
            info!.started_at = date,
            info!.letters = letters;
            info!.owner = user;
            info!.id = info!.id;*/

            var response = await doc.get();
            var responseWithId = response.data();
            responseWithId!.id = response.id;
            responseWithId!.owner = user;
            responseWithId!.started_at = toDateTime(responseWithId!._seconds);
            


            functions.logger.log("RESPONSE START:",responseWithId);

            return res.status(200).json(responseWithId);
        } catch (error) {
            functions.logger.log("body pum",error);
            return res.status(500).json({ "error": JSON.stringify(error)  });

        }
    }, error => {
        return res.status(500).json({ "error": JSON.stringify(error) });

    })
});

router.post("/api/game/finish/:match_id", async (req, res) => {
    checkAuthorization(req).then(async (user: any) => {

        try {

            const doc = await db.collection('games').doc(req.params.match_id);

            var docGet = await doc.get();
            var info = docGet.data();

            if (!docGet.exists) {
                return res.status(404).json({ "error": "Match does not exists" });

            }
            
            var date = new Date();


            
            await doc.update({
                finished: true,
                finished_at: date,
            });



            info!.finished = true;
            info!.finished_at = date;
            

            return res.status(200).json(info);
        } catch (error) {
            functions.logger.log("body pum",error);
            return res.status(500).json({ "error": JSON.stringify(error)  });

        }
    }, error => {
        return res.status(500).json({ "error": JSON.stringify(error) });

    })
});


router.post("/api/game/createMatch", async (req, res) => {

    checkAuthorization(req).then(async (user: any) => {


        try {
            var docRef = await db.collection('games').doc()

            var match = new Match({ owner: user.userId });

            var saveObject = JSON.parse(JSON.stringify(match));

            

            await docRef.create(saveObject);

            var response = await docRef.get();

            var responseWithId = response.data();
            responseWithId!.id = response.id;
            responseWithId!.owner = user;

            return res.status(200).json(responseWithId);
        } catch (error) {
            functions.logger.error("ERROR CREATING GAME", error);

            return res.status(500).send({ "error:": error });
        }
        // ...
    })
        .catch((error: Error) => {
            functions.logger.log("ERROR IS:", error);
            return res.status(500).json({ "error": error.message });
            // Handle error
        });
})










module.exports = router;