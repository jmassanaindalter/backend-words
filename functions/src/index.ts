import * as functions from "firebase-functions";
import * as express from "express";
import * as admin from "firebase-admin";
import * as appfb from "firebase/app";

useEslint: false;

const app = express();

//const db = admin.firestore();


const firebaseConfig = {
      credential: admin.credential.cert("./cifoflutter-982a7-firebase-adminsdk-w7jrr-da9ed6f734.json"),

    apiKey: "AIzaSyA9fi2pYiO4L3lJ0VyFIHCuMlWjPPpaB-I",
    authDomain: "cifoflutter-982a7.firebaseapp.com",
    databaseURL: "https://cifoflutter-982a7-default-rtdb.europe-west1.firebasedatabase.app",
    projectId: "cifoflutter-982a7",
    storageBucket: "cifoflutter-982a7.appspot.com",
    messagingSenderId: "901424687473",
    appId: "1:901424687473:web:c517787f4d0f94bbef6ed0",
    measurementId: "G-JK1DNPBVM9"
  };

admin.initializeApp({
  credential: admin.credential.cert("./cifoflutter-982a7-firebase-adminsdk-w7jrr-da9ed6f734.json"),
  databaseURL: "https://cifoflutter-982a7-default-rtdb.europe-west1.firebasedatabase.app"
});

appfb.initializeApp(firebaseConfig);


app.get('/hello-world',(req,res) => { 
  return res.status(200).json({message:'Hello world'}); 
});

app.use(require('./routes/products.routes'));   
app.use(require('./routes/auth.routes'));   
app.use(require('./routes/users.routes'));   
app.use(require('./routes/books.routes'));   
app.use(require('./routes/game.routes'));   
app.use(require('./routes/dme.routes'));   
app.use(require('./routes/bills.routes'));   

exports.onUserStatusChanged = functions.database.ref('/status/{uid}').onUpdate(
  async (change, context) => {
    functions.logger.log("Changed user status for",change);

    // Get the data written to Realtime Database
    const eventStatus = change.after.val();

    functions.logger.log("Changed user status for",eventStatus);

    // Then use other event data to create a reference to the
    // corresponding Firestore document.
    const userStatusFirestoreRef = admin.firestore().doc(`status/${context.params.uid}`);

    var doc = await admin.firestore().collection('users').doc(context.params.uid);
    
    var online = false;
    if(eventStatus.state == 'online'){
      online = true;
    }


    await doc.update({
      "online":online
    });
    // It is likely that the Realtime Database change that triggered
    // this event has already been overwritten by a fast change in
    // online / offline status, so we'll re-read the current data
    // and compare the timestamps.
    const statusSnapshot = await change.after.ref.once('value');
    const status = statusSnapshot.val();
    functions.logger.log(status, eventStatus);
    // If the current timestamp for this data is newer than
    // the data that triggered this event, we exit this function.
    if (status.last_changed > eventStatus.last_changed) {
      return null;
    }

    // Otherwise, we convert the last_changed field to a Date
    eventStatus.last_changed = new Date(eventStatus.last_changed);

    // ... and write it to Firestore.
    return userStatusFirestoreRef.set(eventStatus);
  });



/*exports.getUsers = functions.https.onCall(async (data, context) => {
  //const text = data.text;
  // Authentication / user information is automatically added to the request.
  //const uid = context.auth?.uid || null;
  //const name = context.auth?.token.name || null;
  //const picture = context.auth?.token.picture || null;
  const email = context.auth?.token.email || null;

  functions.logger.log("LOGGED USER: ",email);
  
  const query  = db.collection('users');
        
  const querySnapshot = await query.get()

  const docs = querySnapshot.docs;

  const response = docs.map(doc=>(doc.data()));

  return response;
});*/

// // Start writing Firebase Functions
// // https://firebase.google.com/docs/functions/typescript
exports.app = functions.https.onRequest(app)

