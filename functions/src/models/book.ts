

export class Book{
    owner:string;
    code:string;
    title:string ;
    coverLink:string;
    status:string;
    availability:number;


    constructor(book:any){
        this.owner = book.owner;
        this.code = book.code ?? "";
        this.title = book.title ?? "unknown";
        this.coverLink = book.coverLink ?? "";
        this.status = book.status ?? "unknown";
        this.availability = book.availability ?? 0;
        
    }


}