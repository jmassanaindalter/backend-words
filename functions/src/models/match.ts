

export class Match{

    owner:string;
    players:any[] = [];
    words:any[] = [];
    started_at?:Date;
    finished_at?:Date;
    started:boolean = false;
    finished:boolean = false;
    letters:string [] = []


    constructor(match:any){
        this.owner = match.owner ?? "";
        this.players = match.players ?? [];
        this.started_at = match.started_at ?? null;
        this.finished_at = match.finished_at ?? null;
        this.started = match.started ?? false;
        this.finished = match.finished ?? false;
        this.letters = match.letters ?? [];
    }


}