import { DmePlayer } from "./player";


export class DmeTeam{
    name:string;
    owner:string;
    players:string[];
    playersPopulated:DmePlayer[];
    points:number;
    category:string;

   

    constructor(team:any){
        this.name = team.name;
        this.owner = team.owner;
        this.players = team.players ?? [];
        this.playersPopulated = team.playersPopulated ?? [];
        this.points = team.points ?? 0;
        this.category = team.category ?? "LOL";
    }


}